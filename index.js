let trainer = {
	name: "Ash Ketchum",
	age: 15,
	pokemon: ['pikachu', 'charizard', 'squirtle', 'bulbasaur'],
	friends: {
		hoenn: ['May', 'Max'],
		kanto: ['Brock', 'Misty']
	},
	talk: function(){
		console.log(this.pokemon[0] + "! I choose you!")
	}
}
console.log("Result of dot notation: ");
console.log(trainer.name);
console.log("Result of square bracket notation: ");
console.log(trainer["pokemon"]);
console.log("Result of talk method");
trainer.talk();
console.log(trainer);

function Pokemon(name, level){
		
		this.pokemonName = name;
		this.pokemonLevel = level;
		this.pokemonHealth = 2 * level;
		this.pokemonAttack = level;

		this.tackle = function(targetPokemon){
			targetPokemon.pokemonHealth -= this.pokemonAttack

			console.log(this.pokemonName + " tackles " + targetPokemon.pokemonName);
			console.log(targetPokemon.pokemonName + " is now reduced to " + targetPokemon.pokemonHealth);

			if(targetPokemon.pokemonHealth <= 0){
				targetPokemon.fainted()
			}
		}

		this.fainted = function(){
			console.log(this.pokemonName + " fainted!");
		}
		}

	let pikachu = new Pokemon("Pikachu", 12);
	console.log(pikachu);
	let gyarados = new Pokemon("Gyarados", 20);
	console.log(gyarados);
	pikachu.tackle(gyarados);
	pikachu.tackle(gyarados);
	pikachu.tackle(gyarados);
	pikachu.tackle(gyarados);